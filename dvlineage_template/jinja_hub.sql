
with 
{% for source in cbc.keyset -%}
    {{ '\n' if not loop.first }}{{source.rel_name}} as (
  select  {{'distinct' if not source.is_unique }}
    {# BK #}
    {%- raw %}{{{% endraw %} dbt.concat([
    {%- for attribute in source.attributes -%}
    {{ ',' if not loop.first }}
        {%- raw %}'{% endraw %}{{ attribute.name }}{% raw %}'{% endraw -%}
    {%- endfor -%}
    ]) {% raw %}}}{% endraw %} as {{source.key_name}}_bk,
    {# HK #}
    {%- raw %}{{{% endraw %} dbt_utils.generate_surrogate_key([
    {%- for attribute in source.attributes -%}
    {{ ',' if not loop.first }}
        {%- raw %}'{% endraw %}{{ attribute.name }}{% raw %}'{% endraw -%}
    {%- endfor -%}
    ]) {% raw %}}}{% endraw %} as {{source.key_name}}_h
  from {% raw %}{{{% endraw %} ref('{{source.rel_name}}') {% raw %}}}{% endraw %} 
){{',' if not loop.last }}
{%- endfor %}

{% for source in cbc.keyset -%}
select * from {{source.rel_name}}
{{' union \n' if not loop.last }}
{%- endfor %}

