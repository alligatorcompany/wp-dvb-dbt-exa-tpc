
with 
{% for item in sources -%}
    {{ '\n' if not loop.first }}{{item.key_name}} as (
  select * from {% raw %}{{{% endraw %} ref('{{item.rel_name}}') {% raw %}}}{% endraw %} 
){{',' if not loop.last }}
{%- endfor -%}
{{ '\n' }}
select
{% for item in sources -%}
    {{ '\n' if not loop.first }}
  {% for attribute in item.attributes -%}
     {{ ',\n  ' if not loop.first }}{{attribute.name}} as {% raw %}'{% endraw %}{{attribute.alias}}{% raw %}'{% endraw %}
  {%- endfor -%}
{{',' if not loop.last }}
{%- endfor %}
from 
{% for jitem in joins -%}
{% for item in jitem -%}
{{item[0]}}{%- if 'query_' not in item[1] and item[1] != '' %}{% raw %}{{{% endraw %}ref('{{item[1]}}'){% raw %}}}{% endraw %} {% endif -%}{{+ ' ' + item[2] + ' ' + item[3] }}{{ '\n' + '  ' *  loop.index }}
{%- endfor %}
{% endfor %}
