
with 
query_00 as (
  select * from {{ ref('H_LINEITEM') }} 
),
query_01 as (
  select * from {{ ref('S_LINEITEM_S_EXA_STAGE_R_Lineitem_U_DBT') }} 
)

select

  query_00.LINEITEM_BK as 'Business Key for Hub Lineitem',

  query_01.L_ORDERKEY as 'L_ORDERKEY',
  query_01.L_LINESTATUS as 'L_LINESTATUS',
  query_01.L_SHIPDATE as 'L_SHIPDATE',
  query_01.L_COMMITDATE as 'L_COMMITDATE',
  query_01.L_RECEIPTDATE as 'L_RECEIPTDATE',
  query_01.L_SHIPINSTRUCT as 'L_SHIPINSTRUCT',
  query_01.L_SHIPMODE as 'L_SHIPMODE',
  query_01.L_COMMENT as 'L_COMMENT',
  query_01.L_PARTKEY as 'L_PARTKEY',
  query_01.L_SUPPKEY as 'L_SUPPKEY',
  query_01.L_LINENUMBER as 'L_LINENUMBER',
  query_01.L_QUANTITY as 'L_QUANTITY',
  query_01.L_EXTENDEDPRICE as 'L_EXTENDEDPRICE',
  query_01.L_DISCOUNT as 'L_DISCOUNT',
  query_01.L_TAX as 'L_TAX',
  query_01.L_RETURNFLAG as 'L_RETURNFLAG'
from 
   query_00 
  
  join  query_01  on query_01.LINEITEM_H = query_00.LINEITEM_H
  
