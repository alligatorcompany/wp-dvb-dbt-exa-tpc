
with 
query_00 as (
  select * from {{ ref('H_REGION') }} 
),
query_01 as (
  select * from {{ ref('S_REGION_S_EXA_STAGE_R_REGION_U_DBT') }} 
)

select

  query_00.REGION_BK as 'Business Key for Hub Region',

  query_01.R_REGIONKEY as 'R_REGIONKEY',
  query_01.R_NAME as 'R_NAME',
  query_01.R_COMMENT as 'R_COMMENT'
from 
   query_00 
  
  join  query_01  on query_01.REGION_H = query_00.REGION_H
  
