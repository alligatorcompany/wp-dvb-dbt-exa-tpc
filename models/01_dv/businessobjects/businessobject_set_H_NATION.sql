
with 
query_00 as (
  select * from {{ ref('H_NATION') }} 
),
query_01 as (
  select * from {{ ref('S_NATION_S_EXA_STAGE_R_NATION_U_DBT') }} 
)

select

  query_00.NATION_BK as 'Business Key for Hub Nation',

  query_01.N_NATIONKEY as 'N_NATIONKEY',
  query_01.N_NAME as 'N_NAME',
  query_01.N_REGIONKEY as 'N_REGIONKEY',
  query_01.N_COMMENT as 'N_COMMENT'
from 
   query_00 
  
  join  query_01  on query_01.NATION_H = query_00.NATION_H
  
