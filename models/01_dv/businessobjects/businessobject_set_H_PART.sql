
with 
query_00 as (
  select * from {{ ref('H_PART') }} 
),
query_01 as (
  select * from {{ ref('S_PART_S_EXA_STAGE_R_PART_U_DBT') }} 
)

select

  query_00.PART_BK as 'Business Key for Hub Part',

  query_01.P_PARTKEY as 'P_PARTKEY',
  query_01.P_NAME as 'P_NAME',
  query_01.P_MFGR as 'P_MFGR',
  query_01.P_BRAND as 'P_BRAND',
  query_01.P_TYPE as 'P_TYPE',
  query_01.P_SIZE as 'P_SIZE',
  query_01.P_CONTAINER as 'P_CONTAINER',
  query_01.P_RETAILPRICE as 'P_RETAILPRICE',
  query_01.P_COMMENT as 'P_COMMENT'
from 
   query_00 
  
  join  query_01  on query_01.PART_H = query_00.PART_H
  
