
with 
query_00 as (
  select * from {{ ref('H_CUSTOMER') }} 
),
query_01 as (
  select * from {{ ref('S_CUSTOMER_S_EXA_STAGE_R_CUSTOMER_U_DBT') }} 
)

select

  query_00.CUSTOMER_BK as 'Business Key for Hub Customer',

  query_01.C_CUSTKEY as 'C_CUSTKEY',
  query_01.C_NAME as 'C_NAME',
  query_01.C_ADDRESS as 'C_ADDRESS',
  query_01.C_NATIONKEY as 'C_NATIONKEY',
  query_01.C_PHONE as 'C_PHONE',
  query_01.C_ACCTBAL as 'C_ACCTBAL',
  query_01.C_MKTSEGMENT as 'C_MKTSEGMENT',
  query_01.C_COMMENT as 'C_COMMENT'
from 
   query_00 
  
  join  query_01  on query_01.CUSTOMER_H = query_00.CUSTOMER_H
  
