
with 
lineitem as (
  select  
    {{ dbt.concat(['L_ORDERKEY','L_LINENUMBER']) }} as lineitem_bk,
    {{ dbt_utils.generate_surrogate_key(['L_ORDERKEY','L_LINENUMBER']) }} as lineitem_h
  from {{ ref('lineitem') }} 
)

select * from lineitem

