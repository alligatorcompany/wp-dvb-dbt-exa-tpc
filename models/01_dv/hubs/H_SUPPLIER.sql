
with 
supplier as (
  select  
    {{ dbt.concat(['S_SUPPKEY']) }} as supplier_bk,
    {{ dbt_utils.generate_surrogate_key(['S_SUPPKEY']) }} as supplier_h
  from {{ ref('supplier') }} 
),
partsupp as (
  select  distinct
    {{ dbt.concat(['PS_SUPPKEY']) }} as supplier_bk,
    {{ dbt_utils.generate_surrogate_key(['PS_SUPPKEY']) }} as supplier_h
  from {{ ref('partsupp') }} 
)

select * from supplier
 union 
select * from partsupp

