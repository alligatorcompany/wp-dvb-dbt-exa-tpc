
with 
partsupp as (
  select  distinct
    {{ dbt.concat(['PS_PARTKEY']) }} as part_bk,
    {{ dbt_utils.generate_surrogate_key(['PS_PARTKEY']) }} as part_h
  from {{ ref('partsupp') }} 
),
part as (
  select  
    {{ dbt.concat(['P_PARTKEY']) }} as part_bk,
    {{ dbt_utils.generate_surrogate_key(['P_PARTKEY']) }} as part_h
  from {{ ref('part') }} 
)

select * from partsupp
 union 
select * from part

