
with 
lineitem as (
  select  distinct
    {{ dbt.concat(['L_PARTKEY','L_SUPPKEY']) }} as partsupp_bk,
    {{ dbt_utils.generate_surrogate_key(['L_PARTKEY','L_SUPPKEY']) }} as partsupp_h
  from {{ ref('lineitem') }} 
),
partsupp as (
  select  
    {{ dbt.concat(['PS_PARTKEY','PS_SUPPKEY']) }} as partsupp_bk,
    {{ dbt_utils.generate_surrogate_key(['PS_PARTKEY','PS_SUPPKEY']) }} as partsupp_h
  from {{ ref('partsupp') }} 
)

select * from lineitem
 union 
select * from partsupp

