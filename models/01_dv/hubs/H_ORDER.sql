
with 
lineitem as (
  select  distinct
    {{ dbt.concat(['L_ORDERKEY']) }} as order_bk,
    {{ dbt_utils.generate_surrogate_key(['L_ORDERKEY']) }} as order_h
  from {{ ref('lineitem') }} 
),
orders as (
  select  
    {{ dbt.concat(['O_ORDERKEY']) }} as order_bk,
    {{ dbt_utils.generate_surrogate_key(['O_ORDERKEY']) }} as order_h
  from {{ ref('orders') }} 
)

select * from lineitem
 union 
select * from orders

