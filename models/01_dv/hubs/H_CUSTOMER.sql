
with 
customer as (
  select  
    {{ dbt.concat(['C_CUSTKEY']) }} as customer_bk,
    {{ dbt_utils.generate_surrogate_key(['C_CUSTKEY']) }} as customer_h
  from {{ ref('customer') }} 
),
orders as (
  select  distinct
    {{ dbt.concat(['O_CUSTKEY']) }} as customer_bk,
    {{ dbt_utils.generate_surrogate_key(['O_CUSTKEY']) }} as customer_h
  from {{ ref('orders') }} 
)

select * from customer
 union 
select * from orders

