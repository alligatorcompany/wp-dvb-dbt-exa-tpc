
with 
lineitem as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['L_PARTKEY','L_SUPPKEY','L_ORDERKEY','L_LINENUMBER']) }} as partsupp_t_lineitem_h,
      
    {{ dbt_utils.generate_surrogate_key(['L_PARTKEY','L_SUPPKEY']) }} as partsupp_h,
    L_PARTKEY as partsupp_L_PARTKEY,
    L_SUPPKEY as partsupp_L_SUPPKEY,
      
    {{ dbt_utils.generate_surrogate_key(['L_ORDERKEY','L_LINENUMBER']) }} as lineitem_h,
    L_ORDERKEY as lineitem_L_ORDERKEY,
    L_LINENUMBER as lineitem_L_LINENUMBER
      
    from {{ ref('lineitem') }} 
)

select * from lineitem
