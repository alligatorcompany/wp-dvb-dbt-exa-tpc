
with 
nation as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['N_REGIONKEY','N_NATIONKEY']) }} as region_t_nation_h,
      
    {{ dbt_utils.generate_surrogate_key(['N_REGIONKEY']) }} as region_h,
    N_REGIONKEY as region_N_REGIONKEY,
      
    {{ dbt_utils.generate_surrogate_key(['N_NATIONKEY']) }} as nation_h,
    N_NATIONKEY as nation_N_NATIONKEY
      
    from {{ ref('nation') }} 
)

select * from nation
