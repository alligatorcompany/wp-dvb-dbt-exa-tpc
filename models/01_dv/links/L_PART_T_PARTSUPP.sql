
with 
partsupp as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['PS_PARTKEY','PS_PARTKEY','PS_SUPPKEY']) }} as part_t_partsupp_h,
      
    {{ dbt_utils.generate_surrogate_key(['PS_PARTKEY']) }} as part_h,
    PS_PARTKEY as part_PS_PARTKEY,
      
    {{ dbt_utils.generate_surrogate_key(['PS_PARTKEY','PS_SUPPKEY']) }} as partsupp_h,
    PS_PARTKEY as partsupp_PS_PARTKEY,
    PS_SUPPKEY as partsupp_PS_SUPPKEY
      
    from {{ ref('partsupp') }} 
)

select * from partsupp
