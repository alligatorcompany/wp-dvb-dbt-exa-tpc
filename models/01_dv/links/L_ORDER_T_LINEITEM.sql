
with 
lineitem as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['L_ORDERKEY','L_ORDERKEY','L_LINENUMBER']) }} as order_t_lineitem_h,
      
    {{ dbt_utils.generate_surrogate_key(['L_ORDERKEY']) }} as order_h,
    L_ORDERKEY as order_L_ORDERKEY,
      
    {{ dbt_utils.generate_surrogate_key(['L_ORDERKEY','L_LINENUMBER']) }} as lineitem_h,
    L_ORDERKEY as lineitem_L_ORDERKEY,
    L_LINENUMBER as lineitem_L_LINENUMBER
      
    from {{ ref('lineitem') }} 
)

select * from lineitem
