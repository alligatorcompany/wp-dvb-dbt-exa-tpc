
with 
region as (
  select  
    {{ dbt_utils.generate_surrogate_key(['R_REGIONKEY'])}} as region_h,
    {{ dbt_utils.generate_surrogate_key(['R_NAME', 'R_COMMENT'])}} as hkdiff,
    R_REGIONKEY,
    R_NAME,
    R_COMMENT
  from {{ ref('region') }} 
)
select * from region
