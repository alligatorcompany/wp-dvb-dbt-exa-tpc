
with 
partsupp as (
  select  
    {{ dbt_utils.generate_surrogate_key(['PS_PARTKEY', 'PS_SUPPKEY'])}} as partsupp_h,
    {{ dbt_utils.generate_surrogate_key(['PS_AVAILQTY', 'PS_SUPPLYCOST', 'PS_COMMENT'])}} as hkdiff,
    PS_PARTKEY,
    PS_SUPPKEY,
    PS_AVAILQTY,
    PS_SUPPLYCOST,
    PS_COMMENT
  from {{ ref('partsupp') }} 
)
select * from partsupp
