
with 
part as (
  select  
    {{ dbt_utils.generate_surrogate_key(['P_PARTKEY'])}} as part_h,
    {{ dbt_utils.generate_surrogate_key(['P_NAME', 'P_MFGR', 'P_BRAND', 'P_TYPE', 'P_SIZE', 'P_CONTAINER', 'P_RETAILPRICE', 'P_COMMENT'])}} as hkdiff,
    P_PARTKEY,
    P_NAME,
    P_MFGR,
    P_BRAND,
    P_TYPE,
    P_SIZE,
    P_CONTAINER,
    P_RETAILPRICE,
    P_COMMENT
  from {{ ref('part') }} 
)
select * from part
