
with 
lineitem as (
  select  
    {{ dbt_utils.generate_surrogate_key(['L_ORDERKEY', 'L_LINENUMBER'])}} as lineitem_h,
    {{ dbt_utils.generate_surrogate_key(['L_PARTKEY', 'L_SUPPKEY', 'L_QUANTITY', 'L_EXTENDEDPRICE', 'L_DISCOUNT', 'L_TAX', 'L_RETURNFLAG', 'L_LINESTATUS', 'L_SHIPDATE', 'L_COMMITDATE', 'L_RECEIPTDATE', 'L_SHIPINSTRUCT', 'L_SHIPMODE', 'L_COMMENT'])}} as hkdiff,
    L_ORDERKEY,
    L_PARTKEY,
    L_SUPPKEY,
    L_LINENUMBER,
    L_QUANTITY,
    L_EXTENDEDPRICE,
    L_DISCOUNT,
    L_TAX,
    L_RETURNFLAG,
    L_LINESTATUS,
    L_SHIPDATE,
    L_COMMITDATE,
    L_RECEIPTDATE,
    L_SHIPINSTRUCT,
    L_SHIPMODE,
    L_COMMENT
  from {{ ref('lineitem') }} 
)
select * from lineitem
