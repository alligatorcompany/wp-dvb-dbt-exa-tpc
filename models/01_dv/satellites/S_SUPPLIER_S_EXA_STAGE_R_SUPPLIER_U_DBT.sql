
with 
supplier as (
  select  
    {{ dbt_utils.generate_surrogate_key(['S_SUPPKEY'])}} as supplier_h,
    {{ dbt_utils.generate_surrogate_key(['S_NAME', 'S_ADDRESS', 'S_NATIONKEY', 'S_PHONE', 'S_ACCTBAL', 'S_COMMENT'])}} as hkdiff,
    S_SUPPKEY,
    S_NAME,
    S_ADDRESS,
    S_NATIONKEY,
    S_PHONE,
    S_ACCTBAL,
    S_COMMENT
  from {{ ref('supplier') }} 
)
select * from supplier
