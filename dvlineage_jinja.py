import csv
import glob
import json
import logging
import os
import pathlib
import sys
from dataclasses import dataclass, field
from typing import Dict, List, Optional

from dataclasses_json import dataclass_json
from jinja2 import Environment, FileSystemLoader, select_autoescape

# from dacite import from_dict


# from dbtparse import DbtProject, DbtProjectType
# from meta import DBMSMeta, Source, Interface
# from dotenv import load_dotenv, find_dotenv


# TEMPLATE_PATH='./exasol/templates'
TEMPLATE_PATH_JINJA = "./dvlineage_template/"
HASHKEY_DATATYPE = "HASHTYPE(16 BYTE)"
HASHKEY_COMMENT = "Hash Key"
BK_DATATYPE = "VARCHAR(2000000) UTF8"
BK_COMMENT = "Business Key"
# TARGET_PATH='./target'
LOG_LEVEL = logging.INFO
LOG = logging.getLogger("dv_lineage")


@dataclass_json
@dataclass
class SourceSystem:
    deployable: bool
    hidden_in_system_deployment: bool
    source_type_id: str
    source_type_name: str
    source_type_url: str
    system_color: str
    system_comment: Optional[str]
    system_id: str
    system_name: str


@dataclass_json
@dataclass
class JdbcDataType:
    character_maximum_length: int
    data_type_name: str
    jdbc_data_type_id: int
    numeric_precision: Optional[int]
    numeric_scale: Optional[int]


@dataclass_json
@dataclass
class Column:
    column_id: str
    column_order: int
    column_name: Optional[str] = None
    complete_data_type: Optional[str] = None
    jdbc_data_type: Optional[JdbcDataType] = None
    column_comment: Optional[str] = None


@dataclass_json
@dataclass
class StagingTable:
    system_id: str
    columns: List[Column]
    loading_batch_size: int
    source_schema_name: str
    source_schema_or_system_id: str
    source_table_id: str
    source_table_name: str
    source_table_staging_id: str
    source_table_type_id: str
    staging_table_comment: Optional[str]
    staging_table_display_string: str
    staging_table_id: str
    staging_table_name: str
    where_clause_delta_part_template: Optional[str]
    where_clause_general_part: Optional[str]
    source_system: Optional[SourceSystem] = None


@dataclass_json
@dataclass
class HubLoad:
    business_key_prefix: str
    business_key_short: str
    column_ids: List[Column]
    datavault_category_id: str
    hub_id: str
    hub_load_display_name: str
    hub_load_id: str
    keys_are_prehashed: bool
    keys_are_unique: bool
    staging_table_id: str
    system_id: str
    system_name: str
    source_system: Optional[SourceSystem] = None
    stage: Optional[StagingTable] = None


@dataclass_json
@dataclass
class LinkLoad:
    link_id: str
    link_load_display_name: str
    link_load_id: str
    staging_table_id: str
    system_id: str
    source_system: Optional[SourceSystem] = None
    stage: Optional[StagingTable] = None


@dataclass_json
@dataclass
class Satellite:
    column_ids: List[Column]
    functional_suffix_id: str
    functional_suffix_name: str
    hub_id: str
    satellite_comment: str
    satellite_id: str
    satellite_is_prototype: bool
    satellite_subject_area_name: str
    staging_resource_id: str
    staging_table_id: str
    system_id: str
    source_system: Optional[SourceSystem] = None
    stage: Optional[StagingTable] = None


@dataclass_json
@dataclass
class Hub:
    hub_comment: Optional[str]
    hub_id: str
    hub_id_of_alias_parent: Optional[str]
    hub_name: str
    hub_name_of_alias_parent: str
    hub_subject_area_name: str
    satellites: List[Satellite] = field(default_factory=list, init=False)
    loads: List[HubLoad] = field(default_factory=list, init=False)


@dataclass_json
@dataclass
class Link:
    hub_a_id: str
    hub_b_id: str
    link_comment: str
    link_id: str
    link_subject_area_name: str
    link_suffix_id: Optional[str]
    link_suffix_name: str
    link_type: str
    loads: List[LinkLoad] = field(default_factory=list, init=False)
    hub_a: Optional[Hub] = None
    hub_b: Optional[Hub] = None


@dataclass_json
@dataclass
class BusinessobjectSet:
    businessobject_set_display_name: str
    businessobject_set_id: str
    functional_suffix_id: str
    functional_suffix_name: str
    start_hub_id: str
    start_hub_name: str


@dataclass_json
@dataclass
class BusinessobjectElement:
    columns: List[Column]
    path_id: str


@dataclass_json
@dataclass
class DependentObject:
    object_id: str
    object_type: Optional[str] = None


@dataclass_json
@dataclass
class Businessobject:
    businessobject_comment: str
    businessobject_display_name: str
    businessobject_dropdown_entry_string: str
    businessobject_elements: List[BusinessobjectElement]
    businessobject_set_id: str
    businessobject_structure: Optional[str]
    businessobject_view_id: str
    dependent_objects: List[DependentObject]
    granularity_satellite_id: Optional[str]
    system_id: str
    system_name: str
    businessobjectsets: List[BusinessobjectSet] = field(
        default_factory=list, init=False
    )


@dataclass
class DVB:
    sources: Dict[str, SourceSystem] = field(default_factory=dict, init=False)
    stages: Dict[str, StagingTable] = field(default_factory=dict, init=False)
    hubs: Dict[str, Hub] = field(default_factory=dict, init=False)
    satellites: Dict[str, Satellite] = field(default_factory=dict, init=False)
    links: Dict[str, Link] = field(default_factory=dict, init=False)
    dimensional_model: Dict[str, Businessobject] = field(
        default_factory=dict, init=False
    )


def parse_dvb_json(
    dvb_meta_path: str,
) -> DVB:
    dv = DVB()
    json_files = glob.glob(dvb_meta_path + "/**/*.json", recursive=True)
    for json in json_files:
        jsonPath = pathlib.PurePath(json)
        type = jsonPath.parent.name
        match type:
            case "hubs":
                fs = open(jsonPath, "r")
                hub = Hub.from_json(fs.read())
                dv.hubs[hub.hub_id] = hub
            case "source_systems":
                fs = open(jsonPath, "r")
                source = SourceSystem.from_json(fs.read())
                dv.sources[source.system_id] = source
            case "dimensional_model":
                fs = open(jsonPath, "r")
                dm = Businessobject.from_json(fs.read())
                dv.dimensional_model[dm.businessobject_set_id] = dm

    for json in json_files:
        jsonPath = pathlib.PurePath(json)
        type = jsonPath.parent.name
        match type:
            case "staging_tables":
                stage = StagingTable.from_json(open(jsonPath, "r").read())
                stage.source_system = dv.sources[stage.system_id]
                dv.stages[stage.staging_table_id] = stage
            case "satellites":
                sat = Satellite.from_json(open(jsonPath, "r").read())
                sat.stage = dv.stages[sat.staging_table_id]
                sat.source_system = dv.sources[sat.system_id]
                dv.hubs[sat.hub_id].satellites.append(sat)
                dv.satellites[sat.satellite_id] = sat
            case "links":
                link = Link.from_json(open(jsonPath, "r").read())
                link.hub_a = dv.hubs[link.hub_a_id]
                link.hub_b = dv.hubs[link.hub_b_id]
                dv.links[link.link_id] = link

    for json in json_files:
        jsonPath = pathlib.PurePath(json)
        type = jsonPath.parent.name
        match type:
            case "hub_loads":
                hubload = HubLoad.from_json(open(jsonPath, "r").read())
                hubload.stage = dv.stages[hubload.staging_table_id]
                hubload.source_system = dv.sources[hubload.system_id]
                hubload.hub = dv.hubs[hubload.hub_id]
                hubload.hub.loads.append(hubload)
            case "link_loads":
                linkload = LinkLoad.from_json(open(jsonPath, "r").read())
                linkload.stage = dv.stages[linkload.staging_table_id]
                linkload.source_system = dv.sources[linkload.system_id]
                linkload.link = dv.links[linkload.link_id]
                linkload.link.loads.append(linkload)
            case "businessobjects_groups":
                bogroup = BusinessobjectSet.from_json(open(jsonPath, "r").read())
                bogroup.businessobject_set_id = dv.dimensional_model[
                    bogroup.businessobject_set_id
                ]

    return dv


@dataclass
class Attribute:
    """
    jedes attribute wird zu einem SQL-Ausdruck in der projektionsliste des zugehörigen select
    a) sql_function as alias -- wenn sql_function und alias gefüllt sind
    b) name as alias -- wenn name and alias gefüllt sind und sql_funciton=None
    c) name - wenn alias=None
    sollte eine sql-function anwendung finden, dann muss auch zwingend ein alias definiert sein
    """

    name: str
    alias: str
    sql_function: str


@dataclass
class BusinessKey:
    """
    jeder bk wird zu einem cte
    CTE_NAME/key_name as (select [distinct/is_unique] (attributes) from rel_name)
    """

    key_name: str
    rel_name: str
    attributes: List[Attribute]
    is_unique: bool


@dataclass
class Context:
    """
    jeder context wird zu eine DBT modell (Satellit), der inkrementell geladen wird
    da wir nur die logische lineage abbilden benötigen wir hier kein scd2
    attributliste distinct slectieren
    +hashkey vom cbc key (BusinessKey)
    +hashdiff aus attributen für vergleich
    +ldts
    +attributes
    """

    context_name: str
    rel_name: str
    key: BusinessKey
    attributes: List[Attribute]


@dataclass
class CBC:
    """
    jede cbc wird zu einem dbt modell mit dem hub namen
    allen bks als cte
    und einem finalen cte aus dem union aller bk-ctes
    select * from bk1.key_name
    union
    select * from bk2.key_name
    ...
    """

    cbc_name: str
    keyset: List[BusinessKey] = field(default_factory=list, init=False)
    context: List[Context] = field(default_factory=list, init=False)


@dataclass
class NBRLoad:
    """
    jeder nbr-load wird zu einer cte
    alle bk müssen sich immer auf eine rel_name beziehen
    CTE_NAME/load_name as (select distinct link-hash,ldts,cbc1_hash(bk1-columns),cbc2_hash(bk2_cols),... from rel_name)

    die bks müssen sich alle auf denselben rel_name, also source/staging beziehen
    """

    load_name: str
    rel_name: str
    cbc_rel: List[BusinessKey]


@dataclass
class NBR:
    """
    jeder nbr wird zu einem DBT modell / link der mit mehreren cte im union beladen wird
    select * from nbrload1.load_name
    union
    select * from nbrload2.load_name
    ...

    jeder nbrload muss die identische bk-list struktur aufweisen (gleiche anzahl von keys)
    """

    nbr_name: str
    nbr_type: str  # n:m, 1:n, n:1
    loads: List[NBRLoad]


@dataclass
class DocsColLoad:
    """
    Loads for BK Columns
    """

    load_name: str
    column_name: List[str]
    column_type: List[str]


@dataclass
class Docs:
    """
    Column Attributes Docs
    """

    file_name: str
    file_comment: str
    column_name: List[str]
    column_type: List[str]
    column_comment: List[str]
    bk_colunm_load: Optional[List[DocsColLoad]] = None


def bo_path(body, path):
    # This is a recursive function to find the path for businesobject
    path_str = path
    listelem = json.loads(json.dumps(body))
    # print(listelem)
    if "hubs" in listelem:
        for lh in listelem["hubs"]:
            path_str = path_str + " --> " + lh["hub_id"]
        return bo_path(lh, path_str)

    if "links" in listelem:
        for ll in listelem["links"]:
            path_str = path_str + " --> " + ll["link_id"]
        return bo_path(ll, path_str)

    if "satellites" in listelem:
        for ls in listelem["satellites"]:
            path_str = path_str + " --> " + ls["satellite_id"]
        return path_str

    return path_str


def main():
    """
    das staging benötigen wir nicht, da nur die logische abbildung der quellen auf die hub/link/satellit
    benötigt werden.

    - zugriffe auf die rel_name Relationen werden ref()-funktionen auf die hr/stg dbt modelle
    - die entity-views müssen ergänzt werden um ref()-funktionen auf die hub/link/satellit modelle

    """

    # Read csv BDV_jobs to now the SR's
    # According DVB Docu --> Partial technical ID of the job : [Source System ID] + "_J_" + [Job Suffix ID]
    sr = []
    bdv_file = "./models/01_dv/dvb/BDV_jobs.csv"
    if os.path.exists(bdv_file):
        with open("./models/01_dv/dvb/BDV_jobs.csv", "r") as csvfile:
            csvreader = csv.reader(csvfile)
            sr = [row[0].upper().split("_J_", 1)[0] for row in csvreader]
            print("./models/01_dv/dvb/BDV_jobs.csv", " is exist")
    else:
        LOG.info("./BDV_jobs.csv", " is not exist")
        print("./BDV_jobs.csv", " is not exist")
    # print(sr)

    dv = parse_dvb_json("./models/01_dv/dvb")

    # Create directories
    dirNames = [
        "./models/01_dv/hubs",
        "./models/01_dv/links",
        "./models/01_dv/satellites",
        "./models/01_dv/businessobjects",
    ]
    for dirName in dirNames:
        try:
            # Create target Directory
            os.mkdir(dirName)
            print("Directory ", dirName, " Created ")
        except FileExistsError:
            LOG.info("!!! Directory ", dirName, " already exists")
            print("Directory ", dirName, " already exists")

    LOG.info("Starting templates:")
    env = Environment(
        loader=FileSystemLoader(TEMPLATE_PATH_JINJA),
        autoescape=select_autoescape(["sql", "yml"]),
    )
    # Create hubs SQL
    for hub in dv.hubs.values():
        if hub.hub_id_of_alias_parent is None:
            filePath = dirNames[0] + "/" + hub.hub_id + ".sql"
            if os.path.exists(filePath):
                os.remove(filePath)
            filePath_doc = dirNames[0] + "/" + hub.hub_id + ".yml"
            if os.path.exists(filePath_doc):
                os.remove(filePath_doc)

            cbc = CBC(hub.hub_id)
            hname = hub.hub_id.lower()

            bks = []
            bk_load = []
            for hubload in hub.loads:
                # exclusion SR von hubs
                if not sr.__contains__(hubload.system_id.upper()):
                    attr = []
                    col_name = []
                    lname = hubload.staging_table_id
                    staging_table = lname[
                        lname.find("_R_") + 3 : lname.rfind("_U_")
                    ].lower()

                    for column in hubload.column_ids:
                        # add only column name
                        # print(column.column_id.split('.')[2])
                        col_name.append(column.column_id.split(".")[2])
                        attr.append(
                            Attribute(column.column_id.split(".")[2], None, None)
                        )

                    bks.append(
                        BusinessKey(
                            hname.split("_", 1)[1],
                            staging_table,
                            attr,
                            hubload.keys_are_unique,
                        )
                    )

                    # define staging table

                    for stage in dv.stages.values():
                        if stage.staging_table_id == lname:
                            bk_col_name = []
                            bk_col_type = []
                            for col in stage.columns:
                                if col.column_name in col_name:
                                    bk_col_name.append(col.column_name)
                                    bk_col_type.append(col.complete_data_type)
                            bk_load.append(
                                DocsColLoad(
                                    stage.staging_table_id, bk_col_name, bk_col_type
                                )
                            )
                    # stage_column = [stage.columns for stage in dv.stages.values() ]

                    # fdocs.bk_colunm_load =  bk_load

            cbc.keyset = bks

            fdocs = Docs(
                hub.hub_id,
                hub.hub_comment,
                [hname.split("_", 1)[1] + "_h", hname.split("_", 1)[1] + "_bk"],
                [HASHKEY_DATATYPE, BK_DATATYPE],
                [HASHKEY_COMMENT, BK_COMMENT],
            )
            fdocs.bk_colunm_load = bk_load
            # print(fdocs.bk_colunm_load)
            for k, v in os.environ.items():
                env.globals[k] = v
            template = env.get_template("jinja_hub.sql").render(cbc=cbc)
            # print(template)
            with open(filePath, mode="w") as f:
                f.write(template)

            # docs yml file
            template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
            with open(filePath_doc, mode="w") as f:
                f.write(template_docs)

    # Create hubs Alias SQL
    for hub in dv.hubs.values():
        if hub.hub_id_of_alias_parent is not None:
            filePath = dirNames[0] + "/" + hub.hub_id + ".sql"
            if os.path.exists(filePath):
                os.remove(filePath)

            filePath_doc = dirNames[0] + "/" + hub.hub_id + ".yml"
            if os.path.exists(filePath_doc):
                os.remove(filePath_doc)

            hname = hub.hub_id.split("_", 1)[1]
            halias = hub.hub_id_of_alias_parent.split("_", 1)[1]

            fdocs = Docs(
                hub.hub_id,
                hub.hub_comment + " Alias of " + halias,
                [hname.lower() + "_h", hname.lower() + "_bk"],
                [HASHKEY_DATATYPE, BK_DATATYPE],
                [HASHKEY_COMMENT, BK_COMMENT],
            )

            for k, v in os.environ.items():
                env.globals[k] = v
            template = env.get_template("jinja_hub_alias.sql").render(
                hname=hname, halias=halias
            )
            # print(template)
            with open(filePath, mode="w") as f:
                f.write(template)

            # docs yml file
            template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
            with open(filePath_doc, mode="w") as f:
                f.write(template_docs)

    # Create links SQL
    for link in dv.links.values():
        filePath = dirNames[1] + "/" + link.link_id + ".sql"
        if os.path.exists(filePath):
            os.remove(filePath)

        filePath_doc = dirNames[1] + "/" + link.link_id + ".yml"
        if os.path.exists(filePath_doc):
            os.remove(filePath_doc)

        fdocs = Docs(
            link.link_id,
            link.link_comment,
            [
                link.link_id.lower()[2:] + "_h",
                link.hub_a_id.lower()[2:] + "_h",
                link.hub_b_id.lower()[2:] + "_h",
            ],
            [HASHKEY_DATATYPE, HASHKEY_DATATYPE, HASHKEY_DATATYPE],
            [HASHKEY_COMMENT, HASHKEY_COMMENT, HASHKEY_COMMENT],
        )
        cbc_list = [link.hub_a_id, link.hub_b_id]

        nbrl = []
        for linkload in link.loads:
            attr = []
            hub_fields = [cbc_hub.split("_", 1)[1] for cbc_hub in cbc_list]
            # name of staging
            lname = linkload.staging_table_id
            staging_table = lname[lname.find("_R_") + 3 : lname.rfind("_U_")].lower()
            lbk = linkload.link_load_display_name.split(" > ")[1]
            lbk = lbk[lbk.find("(") + 1 : lbk.rfind(")")]
            rbk = linkload.link_load_display_name.split(" > ")[2]
            rbk = rbk[rbk.find("(") + 1 : rbk.rfind(")")]
            # add hks
            attr.append(Attribute(lbk + ", " + rbk, linkload.link_id[2:].lower(), None))
            attr.append(Attribute(lbk, hub_fields[0].lower(), None))
            attr.append(Attribute(rbk, hub_fields[1].lower(), None))

            cbc_rel = BusinessKey(None, None, attr, None)
            nbrl.append(NBRLoad(linkload.link_id[2:].lower(), staging_table, cbc_rel))

        nbr = NBR(link.link_id, link.link_type, nbrl)
        for k, v in os.environ.items():
            env.globals[k] = v
        template = env.get_template("jinja_link.sql").render(nbr=nbr)
        with open(filePath, mode="w") as f:
            f.write(template)
        # docs yml file
        template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
        with open(filePath_doc, mode="w") as f:
            f.write(template_docs)

    # Create sats SQL
    for sat in dv.satellites.values():
        filePath = dirNames[2] + "/" + sat.satellite_id + ".sql"
        if os.path.exists(filePath):
            os.remove(filePath)

        filePath_doc = dirNames[2] + "/" + sat.satellite_id + ".yml"
        if os.path.exists(filePath_doc):
            os.remove(filePath_doc)

        cbc = CBC(sat.satellite_id)
        hub = sat.hub_id
        fdocs = Docs(sat.satellite_id, sat.satellite_comment, [], [], [])
        attr = []
        for h in dv.hubs.values():
            if h.hub_id == hub:
                for hubload in h.loads:
                    shk = []
                    if hubload.keys_are_unique:
                        for column in hubload.column_ids:
                            shk.append(column.column_id.split(".", 2)[2])
                        hk = ", ".join(shk)
        # add hk
        hname = sat.hub_id.split("_", 1)[1].lower()
        attr.append(Attribute(hk, hname + "_h", None))
        # add column_type and column_comment
        fdocs.column_name.append(hname + "_h")
        fdocs.column_type.append(HASHKEY_DATATYPE)
        fdocs.column_comment.append(HASHKEY_COMMENT)
        fdocs.column_name.append("hkdiff")
        fdocs.column_type.append(HASHKEY_DATATYPE)
        fdocs.column_comment.append(HASHKEY_COMMENT)

        for column in sat.column_ids:
            if "_offset_sec" in column.column_name:
                sat.column_ids.remove(column)
        # add hkdiff
        sbk = []
        for column in sat.column_ids:
            fdocs.column_name.append(column.column_name)
            if column.column_name in ["TIMESTAMP"]:
                column.column_name = "[" + column.column_name + "]"
            if column.column_name not in hk:
                sbk.append(column.column_name)
        dbk = ", ".join(sbk)
        attr.append(Attribute(dbk, "hkdiff", None))
        con = []
        lname = sat.staging_table_id
        staging_table = lname[lname.find("_R_") + 3 : lname.rfind("_U_")].lower()
        # add rest column
        for column in sat.column_ids:
            attr.append(Attribute(column.column_name, None, None))
            # fdocs.column_name.append( column.column_name)
            fdocs.column_type.append(column.complete_data_type)
            fdocs.column_comment.append(column.column_comment)
        con.append(Context(sat.satellite_id, staging_table, None, attr))
        cbc.context = con

        for k, v in os.environ.items():
            env.globals[k] = v
        template = env.get_template("jinja_sat.sql").render(cbc=cbc)

        with open(filePath, mode="w") as f:
            f.write(template)
        # docs yml file
        template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
        with open(filePath_doc, mode="w") as f:
            f.write(template_docs)

    # Create bos SQL
    for dm in dv.dimensional_model.values():
        filePath = dirNames[3] + "/" + dm.businessobject_set_id + ".sql"
        if os.path.exists(filePath):
            os.remove(filePath)

        filePath_doc = dirNames[3] + "/" + dm.businessobject_set_id + ".yml"
        if os.path.exists(filePath_doc):
            os.remove(filePath_doc)

        # print(dm.businessobject_set_id)
        # print(dm.businessobject_display_name)
        fdocs = Docs(dm.businessobject_set_id, dm.businessobject_comment, [], [], [])
        count = -1
        path_list = []
        sources = []
        joins = []
        for bo in dm.businessobject_elements:
            count += 1
            body = json.loads(bo.path_id)["businessobject_path"]
            bo_str = bo_path(body, "")
            queries = bo_str.split(" --> ")
            path = queries[1:]
            query = queries[-1]
            query_name = "query_" + (str(count) if count > 10 else "0" + str(count))
            field = []
            for col in bo.columns:
                if "_offset_sec" not in col.column_name:
                    col_id = col.column_id.split(".")[2]
                    field.append(
                        Attribute(query_name + "." + col_id, col.column_name, None)
                    )
                    fdocs.column_name.append(col.column_name)
                    if col.column_name[0:3] == "BK_":
                        col.complete_data_type = BK_DATATYPE
                        if col.column_comment == "":
                            col.column_comment = BK_COMMENT
                    if col.column_name[0:3] == "HK_":
                        col.complete_data_type = HASHKEY_DATATYPE
                        col.column_comment = HASHKEY_COMMENT
                    fdocs.column_type.append(col.complete_data_type)
                    fdocs.column_comment.append(col.column_comment)

            sources.append(BusinessKey(query_name, query, field, None))

            for item in path:
                if "L_alias_default" in item:
                    path.remove(item)
            list_join = []
            voralias = ""
            vorrhf = ""
            vortype = ""
            for item in path:
                join = []
                if len(path) == 1:
                    join = ["  ", "", query_name, ""]
                    list_join.append(join)
                    break
                else:
                    type_join = "  left join "
                    if (
                        path.index(item) == len(path) - 1
                        and dm.granularity_satellite_id == path[-1]
                    ):
                        type_join = "  join "
                    tab = ""
                    item_name = path[path.index(item)]
                    item_vorname = path[path.index(item) - 1]
                    item_type = item_name[0:1]
                    if item_type == "L":
                        # lhf = item_name[item_name.find('_T_') + 3:] + '_H'
                        lhf = item_vorname.split("_")[1] + "_H"
                    elif item_type == "S":
                        lhf = (
                            item_name[item_name.find("S_") + 2 : item_name.rfind("_S_")]
                            + "_H"
                        )
                    else:
                        lhf = item_name[2:] + "_H"
                    if path.index(item) == 0:
                        al = "query_00"
                    elif path.index(item) == len(path) - 1:
                        al = query_name
                    else:
                        tab = item
                        al = (
                            query_name
                            + "_"
                            + (
                                str(path.index(item))
                                if path.index(item) > 10
                                else "0" + str(path.index(item))
                            )
                        )
                    rhf = lhf
                    if vortype == item_type:
                        rhf = vorrhf
                    rhf = voralias + "." + rhf
                    vorrhf = lhf
                    vortype = item_type
                    lhf = al + "." + lhf
                    voralias = al
                    join = [type_join, tab, al, " on " + lhf + " = " + rhf]
                    if path.index(item) > 0:
                        list_join.append(join)
            joins.append(list_join)

        joins.insert(1, joins.pop(len(joins) - 1))
        # print(joins)
        for k, v in os.environ.items():
            env.globals[k] = v
        template = env.get_template("jinja_bo.sql").render(sources=sources, joins=joins)
        # print(template)
        with open(filePath, mode="w") as f:
            f.write(template)
        # docs yml file
        template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
        with open(filePath_doc, mode="w") as f:
            f.write(template_docs)


if __name__ == "__main__":
    sys.exit(main())
